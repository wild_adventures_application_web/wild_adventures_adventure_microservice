
CREATE TABLE adventurer (
                id BIGINT NOT NULL,
                CONSTRAINT adventurer_pk PRIMARY KEY (id)
);


CREATE SEQUENCE base_element_id_seq;

CREATE TABLE base_element (
                id BIGINT NOT NULL DEFAULT nextval('base_element_id_seq'),
                name VARCHAR,
                image VARCHAR,
                description VARCHAR,
                CONSTRAINT base_element_pk PRIMARY KEY (id)
);


ALTER SEQUENCE base_element_id_seq OWNED BY base_element.id;

CREATE TABLE category (
                id BIGINT NOT NULL,
                CONSTRAINT category_pk PRIMARY KEY (id)
);


CREATE TABLE adventure (
                id BIGINT NOT NULL,
                category_id BIGINT,
                price INTEGER,
                CONSTRAINT adventure_pk PRIMARY KEY (id)
);


CREATE TABLE adventure_event (
                id BIGINT NOT NULL,
                adventure_id BIGINT NOT NULL,
                date TIMESTAMP,
                CONSTRAINT adventure_event_pk PRIMARY KEY (id)
);


CREATE TABLE adventure_event_adventurer (
                adventure_event_id BIGINT NOT NULL,
                adventurer_id BIGINT NOT NULL,
                CONSTRAINT adventure_event_adventurer_pk PRIMARY KEY (adventure_event_id, adventurer_id)
);


ALTER TABLE adventure_event_adventurer ADD CONSTRAINT adventurer_adventure_event_adventurer_fk
FOREIGN KEY (adventurer_id)
REFERENCES adventurer (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE category ADD CONSTRAINT base_element_category_fk
FOREIGN KEY (id)
REFERENCES base_element (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE adventure ADD CONSTRAINT base_element_adventure_fk
FOREIGN KEY (id)
REFERENCES base_element (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE adventure_event ADD CONSTRAINT base_element_adventure_event_fk
FOREIGN KEY (id)
REFERENCES base_element (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE adventure ADD CONSTRAINT category_adventure_fk
FOREIGN KEY (category_id)
REFERENCES category (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE adventure_event ADD CONSTRAINT adventure_adventure_event_fk
FOREIGN KEY (adventure_id)
REFERENCES adventure (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE adventure_event_adventurer ADD CONSTRAINT adventure_event_adventure_event_adventurer_fk
FOREIGN KEY (adventure_event_id)
REFERENCES adventure_event (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;