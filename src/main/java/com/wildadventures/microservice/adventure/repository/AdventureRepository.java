package com.wildadventures.microservice.adventure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wildadventures.microservice.adventure.domain.Adventure;

@Repository
public interface AdventureRepository extends JpaRepository<Adventure, Long> {

}
