package com.wildadventures.microservice.adventure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wildadventures.microservice.adventure.domain.AdventureEvent;

@Repository
public interface AdventureEventRepository extends JpaRepository<AdventureEvent, Long> {

}
