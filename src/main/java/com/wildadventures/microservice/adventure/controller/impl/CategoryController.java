package com.wildadventures.microservice.adventure.controller.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wildadventures.microservice.adventure.service.EntityService;
import com.wildadventures.microservice.adventure.controller.EntityController;
import com.wildadventures.microservice.adventure.domain.Category;

@RestController
@RequestMapping(path = "/categories", produces = "application/json")
public class CategoryController implements EntityController<Category> {

	@Autowired
	private EntityService<Category> categoryService;
	
	private static Logger logger = LoggerFactory.getLogger(CategoryController.class);

	@GetMapping("/{id}")
	public ResponseEntity<Category> getOne(
			@PathVariable Long id, 
			@RequestParam(required = false, defaultValue = "false") boolean comments) {
		Category category = this.categoryService.findOne(id, comments);

		return new ResponseEntity<Category>(category, HttpStatus.OK);
	}

	@GetMapping
	@Override
	public ResponseEntity<List<Category>> getMany(@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer size,
			@RequestParam(required = false, defaultValue = "false") boolean filter,
			@RequestParam(required = false) String name, @RequestParam(required = false) Long parentId) {
		logger.info("Handling getMany...");
		
		Category category = new Category();
		category.setName(name);

		List<Category> categories = this.categoryService.handleFind(page, size, filter, category);

		return new ResponseEntity<List<Category>>(categories, HttpStatus.OK);
	}

	@PostMapping
	@Override
	public ResponseEntity<Void> postMany(@RequestBody List<Category> categories) {
		this.categoryService.insertMany(categories);

		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@PutMapping
	@Override
	public ResponseEntity<Void> putMany(@RequestBody List<Category> categories) {
		this.categoryService.updateMany(categories);

		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	@DeleteMapping
	@Override
	public ResponseEntity<Void> deleteMany(@RequestBody List<Category> categories) {
		this.categoryService.deleteMany(categories);

		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
}
