package com.wildadventures.microservice.adventure.controller.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wildadventures.microservice.adventure.controller.EntityController;
import com.wildadventures.microservice.adventure.domain.Adventure;
import com.wildadventures.microservice.adventure.domain.AdventureEvent;
import com.wildadventures.microservice.adventure.service.EntityService;

@RestController
@RequestMapping(path = "/adventure-events", produces = "application/json")
public class AdventureEventController implements EntityController<AdventureEvent> {

	@Autowired
	private EntityService<AdventureEvent> adventureEventService;

	@GetMapping("/{id}")
	@Override
	public ResponseEntity<AdventureEvent> getOne(
			@PathVariable Long id,
			@RequestParam(required = false, defaultValue = "false") boolean comments) {
		AdventureEvent adventureEvent = this.adventureEventService.findOne(id, comments);

		return new ResponseEntity<AdventureEvent>(adventureEvent, HttpStatus.OK);
	}

	@GetMapping
	@Override
	public ResponseEntity<List<AdventureEvent>> getMany(@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer size,
			@RequestParam(required = false, defaultValue = "false") boolean filter,
			@RequestParam(required = false) String name, @RequestParam(required = false) Long parentId) {
		AdventureEvent entity = new AdventureEvent();
		entity.setName(name);
		if (parentId != null) {
			entity.setAdventure(new Adventure(parentId));
		}

		List<AdventureEvent> entities = this.adventureEventService.handleFind(page, size, filter, entity);

		return new ResponseEntity<List<AdventureEvent>>(entities, HttpStatus.OK);
	}

	@PostMapping
	@Override
	public ResponseEntity<Void> postMany(@RequestBody List<AdventureEvent> entities) {
		this.adventureEventService.insertMany(entities);

		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@PutMapping
	@Override
	public ResponseEntity<Void> putMany(@RequestBody List<AdventureEvent> entities) {
		this.adventureEventService.updateMany(entities);

		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	@DeleteMapping
	@Override
	public ResponseEntity<Void> deleteMany(@RequestBody List<AdventureEvent> entities) {
		this.adventureEventService.deleteMany(entities);

		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

}
