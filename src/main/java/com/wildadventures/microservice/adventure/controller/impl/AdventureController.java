package com.wildadventures.microservice.adventure.controller.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wildadventures.microservice.adventure.controller.EntityController;
import com.wildadventures.microservice.adventure.domain.Adventure;
import com.wildadventures.microservice.adventure.domain.Category;
import com.wildadventures.microservice.adventure.service.EntityService;

@RestController
@RequestMapping(path = "/adventures", produces = "application/json")
public class AdventureController implements EntityController<Adventure> {

	@Autowired
	private EntityService<Adventure> adventureService;
	

	@GetMapping("/{id}")
	@Override
	public ResponseEntity<Adventure> getOne(
			@PathVariable Long id, 
			@RequestParam(required = false, defaultValue = "false") boolean comments) {
		Adventure adventure = this.adventureService.findOne(id, comments);

		return new ResponseEntity<Adventure>(adventure, HttpStatus.OK);
	}

	@GetMapping
	@Override
	public ResponseEntity<List<Adventure>> getMany(@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer size,
			@RequestParam(required = false, defaultValue = "false") boolean filter,
			@RequestParam(required = false) String name, @RequestParam(required = false) Long parentId) {
		Adventure adventure = new Adventure();
		adventure.setName(name);
		if (parentId != null) {
			adventure.setCategory(new Category(parentId));
		}

		List<Adventure> adventures = this.adventureService.handleFind(page, size, filter, adventure);

		return new ResponseEntity<List<Adventure>>(adventures, HttpStatus.OK);
	}

	@PostMapping
	@Override
	public ResponseEntity<Void> postMany(@RequestBody List<Adventure> adventures) {
		this.adventureService.insertMany(adventures);

		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@PutMapping
	@Override
	public ResponseEntity<Void> putMany(@RequestBody List<Adventure> adventures) {
		this.adventureService.updateMany(adventures);

		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	@DeleteMapping
	@Override
	public ResponseEntity<Void> deleteMany(@RequestBody List<Adventure> adventures) {
		this.adventureService.deleteMany(adventures);

		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
}
