package com.wildadventures.microservice.adventure.domain;

import javax.persistence.*;

import lombok.Data;

import java.util.List;


/**
 * The persistent class for the category database table.
 * 
 */
@Data
@Entity
@Table(name="category")
public class Category extends BaseElement {
	
	@Transient
	private List<Adventure> adventures;

	public Category() {}
	
	public Category(Long id) {
		super(id);
	}

	
}