package com.wildadventures.microservice.adventure.domain;

import javax.persistence.*;

import lombok.Data;

import java.util.List;


/**
 * The persistent class for the adventurer database table.
 * 
 */
@Data
@Entity
public class Adventurer {

	@Id
	private Long id;

	//bi-directional many-to-many association to AdventureEvent
	@Transient
	@ManyToMany(mappedBy="adventurers")
	private List<AdventureEvent> adventureEvents;

}