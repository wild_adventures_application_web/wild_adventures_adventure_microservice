package com.wildadventures.microservice.adventure.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
@Table(name="base_element")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class BaseElement {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	protected Long id;
	protected String description;
	protected String image;
	protected String name;
	
	@Transient
	protected List<Comment> comments = new ArrayList<Comment>();
	
	protected BaseElement() {}
	
	protected BaseElement(Long id) {
		this.id = id;
	}
}