package com.wildadventures.microservice.adventure.domain;

import javax.persistence.*;

import lombok.Data;

import java.util.List;


/**
 * The persistent class for the adventure database table.
 * 
 */
@Data
@Entity
@Table(name="adventure")
public class Adventure extends BaseElement {

	private Integer price;

	//bi-directional many-to-one association to Category
	@ManyToOne
	@JoinColumn(name = "category_id")
	private Category category;

	@Transient
	private List<AdventureEvent> adventureEvents;
	
	public Adventure() {}

	public Adventure(Long id) {
		super(id);
	}

}