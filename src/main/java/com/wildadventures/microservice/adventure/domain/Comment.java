package com.wildadventures.microservice.adventure.domain;

import lombok.Data;

/**
 * The comment will be fetch in comment-micro-service.
 * 
 */
@Data
public class Comment {

	private Long id;
	private String content;
	private Long userId;
	private Long itemId;

}