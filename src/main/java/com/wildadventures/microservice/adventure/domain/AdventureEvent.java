package com.wildadventures.microservice.adventure.domain;

import javax.persistence.*;

import lombok.Data;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the adventure_event database table.
 * 
 */
@Data
@Entity
@Table(name="adventure_event")
public class AdventureEvent extends BaseElement {

	private Timestamp date;

	//bi-directional many-to-one association to Adventure
	@ManyToOne
	private Adventure adventure;

	//bi-directional many-to-many association to Adventurer
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
		name="adventure_event_adventurer"
		, joinColumns={
			@JoinColumn(name="adventure_event_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="adventurer_id")
			}
		)
	private List<Adventurer> adventurers;

	public AdventureEvent() {}	
	
	public AdventureEvent(Long id) {
		super(id);
	}	
	
}
