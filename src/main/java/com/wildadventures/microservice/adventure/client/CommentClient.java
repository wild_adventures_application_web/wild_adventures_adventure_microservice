package com.wildadventures.microservice.adventure.client;

import com.wildadventures.microservice.adventure.domain.Comment;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Repository
@FeignClient("comment-microservice")
@RequestMapping("${comment-microservice.context-path}")
public interface CommentClient {

	@GetMapping(path = "/comments?filter=true")
	List<Comment> getComments(@RequestParam("itemId") Long itemId);

}
