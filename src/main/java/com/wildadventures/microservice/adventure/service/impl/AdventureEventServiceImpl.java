package com.wildadventures.microservice.adventure.service.impl;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.wildadventures.microservice.adventure.client.CommentClient;
import com.wildadventures.microservice.adventure.domain.AdventureEvent;
import com.wildadventures.microservice.adventure.domain.Adventurer;
import com.wildadventures.microservice.adventure.domain.Comment;
import com.wildadventures.microservice.adventure.exception.BadRequestException;
import com.wildadventures.microservice.adventure.exception.NotFoundException;
import com.wildadventures.microservice.adventure.repository.AdventureEventRepository;
import com.wildadventures.microservice.adventure.repository.AdventurerRepository;
import com.wildadventures.microservice.adventure.service.EntityService;

@Service
public class AdventureEventServiceImpl implements EntityService<AdventureEvent> {

	@Autowired
	private AdventureEventRepository adventureEventRepository;
	
	@Autowired
	private AdventurerRepository adventurerRepository;

	@Autowired
	private CommentClient commentClient;
	
	Logger logger = LoggerFactory.getLogger(AdventureEventServiceImpl.class);

	@Override
	public AdventureEvent findOne(long id, boolean addComments) {
		Optional<AdventureEvent> optAdventureEvent = this.adventureEventRepository.findById(id);
		String errorMsg = String.format("No adventure-event with id %d.", id);

		if (addComments) {
			optAdventureEvent.ifPresent(adventureEvent -> {
				try {
					List<Comment> comments = commentClient.getComments(adventureEvent.getId());
					adventureEvent.setComments(comments);
				} catch (Exception e) {
					logger.warn(String.format("An error occured while trying to fetch comments for category with Id: %d", adventureEvent.getId()));
					logger.debug(ExceptionUtils.getStackTrace(e));
				}
			});
		}
		
		return optAdventureEvent.orElseThrow(() -> new NotFoundException(errorMsg));
	}

	@Override
	public List<AdventureEvent> handleFind(Integer page, Integer size, Boolean filter, AdventureEvent entity) {
		List<AdventureEvent> entities;

		ExampleMatcher matcher = ExampleMatcher.matching().withIgnoreCase()
				.withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);

		if (!filter && (page == null || size == null)) {
			entities = this.adventureEventRepository.findAll();
		} else if (!filter && (page != null && size != null)) {
			entities = this.adventureEventRepository.findAll(PageRequest.of(page, size)).getContent();
		} else if (filter && (page == null || size == null)) {
			entities = this.adventureEventRepository.findAll(Example.of(entity, matcher));
		} else {
			entities = this.adventureEventRepository.findAll(Example.of(entity, matcher), PageRequest.of(page, size))
					.getContent();
		}

		if (entities.isEmpty()) {
			throw new NotFoundException("No adventureEvents found.");
		}

		return entities;
	}

	@Override
	public void insertMany(List<AdventureEvent> entities) {
		String errorMsg;

		if (entities.isEmpty()) {
			errorMsg = "No adventure-events provided.";

			throw new BadRequestException(errorMsg);
		}

		for (AdventureEvent entity : entities) {
			if (entity.getId() != null) {
				throw new BadRequestException("The ids are auto-generated, hint: use PUT to update an entity.");
			}
			
			this.saveMissingAdventurers(entity.getAdventurers());
		}

		this.adventureEventRepository.saveAll(entities);
	}

	@Override
	public void updateMany(List<AdventureEvent> entities) {
		String errorMsg;

		if (entities.isEmpty()) {
			errorMsg = "No adventure-events provided.";

			throw new BadRequestException(errorMsg);
		}

		for (AdventureEvent entity : entities) {
			if (entity.getId() == null) {
				errorMsg = "An adventureEvent has no Id. Hint: use POST to insert an entity.";
				throw new BadRequestException(errorMsg);
			} else if (!this.adventureEventRepository.existsById(entity.getId())) {
				errorMsg = String.format("No adventure-event with id %d.", entity.getId());
				throw new NotFoundException(errorMsg);
			}
			
			this.saveMissingAdventurers(entity.getAdventurers());
		}

		this.adventureEventRepository.saveAll(entities);
	}

	@Override
	public void deleteMany(List<AdventureEvent> entities) {
		String errorMsg;

		if (entities.isEmpty()) {
			errorMsg = "No adventure-events provided.";

			throw new BadRequestException(errorMsg);
		}

		for (AdventureEvent entity : entities) {
			if (entity.getId() == null) {
				errorMsg = "An adventure-event has no Id. Cannot delete an adventure if no id is provided.";
				throw new BadRequestException(errorMsg);
			} else if (!this.adventureEventRepository.existsById(entity.getId())) {
				errorMsg = String.format("No adventure-event with id %d.", entity.getId());
				throw new NotFoundException(errorMsg);
			}
		}

		this.adventureEventRepository.deleteInBatch(entities);
	}

	private void saveMissingAdventurers(List<Adventurer> adventurers) {
		if(adventurers == null) {
			return;
		}
		
		for(Adventurer adventurer : adventurers) {
			if (!this.adventurerRepository.existsById(adventurer.getId())) {
				this.adventurerRepository.save(adventurer);
			}
		}			
	}

}
