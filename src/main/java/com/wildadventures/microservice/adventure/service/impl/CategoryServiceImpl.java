package com.wildadventures.microservice.adventure.service.impl;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.wildadventures.microservice.adventure.client.CommentClient;
import com.wildadventures.microservice.adventure.domain.Category;
import com.wildadventures.microservice.adventure.domain.Comment;
import com.wildadventures.microservice.adventure.exception.BadRequestException;
import com.wildadventures.microservice.adventure.exception.NotFoundException;
import com.wildadventures.microservice.adventure.repository.CategoryRepository;
import com.wildadventures.microservice.adventure.service.EntityService;

@Service
public class CategoryServiceImpl implements EntityService<Category> {

	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private CommentClient commentClient;
	
	Logger logger = LoggerFactory.getLogger(CategoryServiceImpl.class);
	
	@Override
	public Category findOne(long categoryId, boolean addComments) {
		Optional<Category> optCategory = this.categoryRepository.findById(categoryId);
		String errorMsg = String.format("No category with id %d.", categoryId);

		if (addComments) {
			optCategory.ifPresent(category -> {
				try {
					List<Comment> comments = commentClient.getComments(category.getId());
					category.setComments(comments);					
				} catch (Exception e) {
					logger.warn(String.format("An error occured while trying to fetch comments for category with Id: %d", category.getId()));
					logger.debug(ExceptionUtils.getStackTrace(e));
				}
			});
		}
		
		return optCategory.orElseThrow(() -> new NotFoundException(errorMsg));
	}


	@Override
	public List<Category> handleFind(Integer page, Integer size, Boolean filter, Category entity) {
		List<Category> categories;

		ExampleMatcher matcher = ExampleMatcher.matching().withIgnoreCase()
				.withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);

		if (!filter && (page == null || size == null)) {
			categories = this.categoryRepository.findAll();
		} else if (!filter && (page != null && size != null)) {
			categories = this.categoryRepository.findAll(PageRequest.of(page, size)).getContent();
		} else if (filter && (page == null || size == null)) {
			categories = this.categoryRepository.findAll(Example.of(entity, matcher));
		} else {
			categories = this.categoryRepository.findAll(Example.of(entity, matcher), PageRequest.of(page, size))
					.getContent();
		}
		
		if (categories.isEmpty()) {
			throw new NotFoundException("No categories found.");
		}
		
		return categories;
	};

	@Override
	public void insertMany(List<Category> categories) {
		String errorMsg;

		if (categories.isEmpty()) {
			errorMsg = "No adventures provided.";

			throw new BadRequestException(errorMsg);
		}

		for (Category category : categories) {
			if (category.getId() != null) {
				throw new BadRequestException("The ids are auto-generated, hint: use PUT to update an entity.");
			}
		}

		this.categoryRepository.saveAll(categories);
	}

	@Override
	public void updateMany(List<Category> categories) {
		String errorMsg;

		if (categories.isEmpty()) {
			errorMsg = "No adventures provided.";

			throw new BadRequestException(errorMsg);
		}

		for (Category category : categories) {
			if (category.getId() == null) {
				errorMsg = "A category has no Id. Hint: use POST to insert an entity.";
				throw new BadRequestException(errorMsg);
			} else if (!this.categoryRepository.existsById(category.getId())) {
				errorMsg = String.format("No category with id %d.", category.getId());
				throw new NotFoundException(errorMsg);
			}
		}

		this.categoryRepository.saveAll(categories);

	}

	@Override
	public void deleteMany(List<Category> categories) {
		String errorMsg;

		if (categories.isEmpty()) {
			errorMsg = "No adventures provided.";

			throw new BadRequestException(errorMsg);
		}

		for (Category category : categories) {
			if (category.getId() == null) {
				errorMsg = "An adventure has no Id. Cannot delete a category if no id is provided.";
				throw new BadRequestException(errorMsg);
			} else if (!this.categoryRepository.existsById(category.getId())) {
				errorMsg = String.format("No category with id %d.", category.getId());
				throw new NotFoundException(errorMsg);
			}
		}

		this.categoryRepository.deleteInBatch(categories);

	}

}
