package com.wildadventures.microservice.adventure.service;

import java.util.List;

public interface EntityService<T> {
	
	T findOne(long id, boolean comments);
		
	List<T> handleFind(Integer page, Integer size, Boolean filter, T entity);

	void insertMany(List<T> entities);

	void updateMany(List<T> entities);

	void deleteMany(List<T> entities);





}
