package com.wildadventures.microservice.adventure.service.impl;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.wildadventures.microservice.adventure.client.CommentClient;
import com.wildadventures.microservice.adventure.domain.Adventure;
import com.wildadventures.microservice.adventure.domain.Comment;
import com.wildadventures.microservice.adventure.exception.BadRequestException;
import com.wildadventures.microservice.adventure.exception.NotFoundException;
import com.wildadventures.microservice.adventure.repository.AdventureRepository;
import com.wildadventures.microservice.adventure.service.EntityService;

@Service
public class AdventureServiceImpl implements EntityService<Adventure> {

	@Autowired
	private AdventureRepository adventureRepository;
	
	@Autowired
	private CommentClient commentClient;
	
	Logger logger = LoggerFactory.getLogger(AdventureServiceImpl.class);

	@Override
	public Adventure findOne(long adventureId, boolean addComments) {
		Optional<Adventure> optAdventure = this.adventureRepository.findById(adventureId);
		String errorMsg = String.format("No adventure with id %d.", adventureId);

		if (addComments) {
			optAdventure.ifPresent(adventure -> {
				try {
					List<Comment> comments = commentClient.getComments(adventure.getId());
					adventure.setComments(comments);			
				} catch (Exception e) {
					logger.warn(String.format("An error occured while trying to fetch comments for category with Id: %d", adventure.getId()));
					logger.debug(ExceptionUtils.getStackTrace(e));
				}
			});
		}
		
		return optAdventure.orElseThrow(() -> new NotFoundException(errorMsg));
	}

	@Override
	public List<Adventure> handleFind(Integer page, Integer size, Boolean filter, Adventure entity) {
		List<Adventure> adventures;

		ExampleMatcher matcher = ExampleMatcher.matching().withIgnoreCase()
				.withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);

		if (!filter && (page == null || size == null)) {
			adventures = this.adventureRepository.findAll();
		} else if (!filter && (page != null && size != null)) {
			adventures = this.adventureRepository.findAll(PageRequest.of(page, size)).getContent();
		} else if (filter && (page == null || size == null)) {
			adventures = this.adventureRepository.findAll(Example.of(entity, matcher));
		} else {
			adventures = this.adventureRepository.findAll(Example.of(entity, matcher), PageRequest.of(page, size))
					.getContent();
		}
		
		if (adventures.isEmpty()) {
			throw new NotFoundException("No adventures found.");
		}
		
		return adventures;
	}

	@Override
	public void insertMany(List<Adventure> adventures) {
		String errorMsg;

		if (adventures.isEmpty()) {
			errorMsg = "No adventures provided.";

			throw new BadRequestException(errorMsg);
		}

		for (Adventure adventure : adventures) {
			if (adventure.getId() != null) {
				throw new BadRequestException("The ids are auto-generated, hint: use PUT to update an entity.");
			}
		}

		this.adventureRepository.saveAll(adventures);
	}

	@Override
	public void updateMany(List<Adventure> adventures) {
		String errorMsg;

		if (adventures.isEmpty()) {
			errorMsg = "No adventures provided.";

			throw new BadRequestException(errorMsg);
		}

		for (Adventure adventure : adventures) {
			if (adventure.getId() == null) {
				errorMsg = "An adventure has no Id. Hint: use POST to insert an entity.";
				throw new BadRequestException(errorMsg);
			} else if (!this.adventureRepository.existsById(adventure.getId())) {
				errorMsg = String.format("No adventure with id %d.", adventure.getId());
				throw new NotFoundException(errorMsg);
			}
		}

		this.adventureRepository.saveAll(adventures);
	}

	@Override
	public void deleteMany(List<Adventure> adventures) {
		String errorMsg;

		if (adventures.isEmpty()) {
			errorMsg = "No adventures provided.";

			throw new BadRequestException(errorMsg);
		}

		for (Adventure adventure : adventures) {
			if (adventure.getId() == null) {
				errorMsg = "An adventure has no Id. Cannot delete an adventure if no id is provided.";
				throw new BadRequestException(errorMsg);
			} else if (!this.adventureRepository.existsById(adventure.getId())) {
				errorMsg = String.format("No adventure with id %d.", adventure.getId());
				throw new NotFoundException(errorMsg);
			}
		}

		this.adventureRepository.deleteInBatch(adventures);
	}
}
